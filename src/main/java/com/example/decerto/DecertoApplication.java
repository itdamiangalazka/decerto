package com.example.decerto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DecertoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DecertoApplication.class, args);
    }

}
