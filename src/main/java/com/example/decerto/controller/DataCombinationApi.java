package com.example.decerto.controller;

import com.example.decerto.data.CombinationResultDto;
import com.example.decerto.service.DataCombinationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
public abstract class DataCombinationApi<T extends CombinationResultDto> {

    private final DataCombinationService<T> dataCombinationService;

    @GetMapping("/combine")
    public String combine() {
        T combineResult = dataCombinationService.combine();
        return String.valueOf(combineResult.getValue());
    }
}
