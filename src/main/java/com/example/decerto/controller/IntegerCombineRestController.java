package com.example.decerto.controller;

import com.example.decerto.data.IntegerCombinationResultDto;
import com.example.decerto.service.IntegerCombinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.example.decerto.controller.IntegerCombineRestController.INTEGER_URI;

@RestController
@RequestMapping(INTEGER_URI)
public class IntegerCombineRestController extends DataCombinationApi<IntegerCombinationResultDto> {

    protected final static String INTEGER_URI = "/int";

    @Autowired
    public IntegerCombineRestController(IntegerCombinationService integerCombinationService) {
        super(integerCombinationService);
    }
}
