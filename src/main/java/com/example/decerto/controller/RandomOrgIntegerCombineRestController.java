package com.example.decerto.controller;

import com.example.decerto.service.RandomOrgIntegerCombinationService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/randomorg" + IntegerCombineRestController.INTEGER_URI)
public class RandomOrgIntegerCombineRestController extends IntegerCombineRestController {

    public RandomOrgIntegerCombineRestController(RandomOrgIntegerCombinationService integerCombinationService) {
        super(integerCombinationService);
    }
}
