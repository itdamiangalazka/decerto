package com.example.decerto.service;

import com.example.decerto.data.CombinationDataSource;
import com.example.decerto.data.RandomOrgIntegerGenerator;
import org.springframework.stereotype.Service;

@Service
public class RandomOrgIntegerCombinationService extends IntegerCombinationService {

    @Override
    public CombinationDataSource<Integer> getCombinationDataSource() {
        return new RandomOrgIntegerGenerator(1, 100); // min, max can be passed by api or from config
    }
}
