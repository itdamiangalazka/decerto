package com.example.decerto.service;

import com.example.decerto.data.CombinationDataSource;
import com.example.decerto.data.CombinationResultDto;
import com.example.decerto.strategy.CombinationStrategy;

public abstract class AbstractDataCombinationService<T extends CombinationResultDto, K> implements DataCombinationService<T> {

    public abstract CombinationStrategy<T, K> getCombinationStrategy();
    public abstract CombinationDataSource<K> getCombinationDataSource();

    @Override
    public T combine() {
        CombinationStrategy<T, K> combinationStrategy = getCombinationStrategy();
        CombinationDataSource<K> combinationDataSource = getCombinationDataSource();
        K firstData = combinationDataSource.getData();
        K secondData = combinationDataSource.getData();
        return combinationStrategy.combine(firstData, secondData);
    }
}
