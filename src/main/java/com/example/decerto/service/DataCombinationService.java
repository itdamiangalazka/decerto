package com.example.decerto.service;

import com.example.decerto.data.CombinationResultDto;

public interface DataCombinationService<T extends CombinationResultDto> {

    T combine();
}
