package com.example.decerto.service;

import com.example.decerto.data.CombinationDataSource;
import com.example.decerto.data.IntegerCombinationResultDto;
import com.example.decerto.data.RandomIntegerGenerator;
import com.example.decerto.strategy.CombinationStrategy;
import com.example.decerto.strategy.IntegerCombinationStrategy;
import org.springframework.stereotype.Service;

@Service
public class IntegerCombinationService extends AbstractDataCombinationService<IntegerCombinationResultDto, Integer> {

    @Override
    public CombinationStrategy<IntegerCombinationResultDto, Integer> getCombinationStrategy() {
        return new IntegerCombinationStrategy();
    }

    @Override
    public CombinationDataSource<Integer> getCombinationDataSource() {
        return new RandomIntegerGenerator();
    }
}
