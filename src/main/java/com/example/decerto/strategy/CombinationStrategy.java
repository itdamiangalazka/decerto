package com.example.decerto.strategy;

import com.example.decerto.data.CombinationResultDto;

public interface CombinationStrategy<T extends CombinationResultDto, K> {

    T combine(K first, K second);
}
