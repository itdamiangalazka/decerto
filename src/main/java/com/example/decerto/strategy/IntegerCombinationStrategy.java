package com.example.decerto.strategy;

import com.example.decerto.data.IntegerCombinationResultDto;

public class IntegerCombinationStrategy implements CombinationStrategy<IntegerCombinationResultDto, Integer> {

    @Override
    public IntegerCombinationResultDto combine(Integer first, Integer second) {
        return new IntegerCombinationResultDto(first + second);
    }
}
