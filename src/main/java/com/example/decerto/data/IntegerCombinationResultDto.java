package com.example.decerto.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class IntegerCombinationResultDto extends CombinationResultDto {

    private Integer value;
}
