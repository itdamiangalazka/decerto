package com.example.decerto.data;

import com.example.decerto.utils.HttpRequestUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class RandomOrgIntegerGenerator implements CombinationDataSource<Integer> {

    private static final String RANDOM_ORG_API_URL = "https://www.random.org/integers/?";

    /**
     * Parameters hardcoded in demo version
     */
    private static final String DEFAULT_NUM = "1";
    private static final String DEFAULT_COL = "1";
    private static final String DEFAULT_BASE = "10";
    private static final String DEFAULT_FORMAT = "plain";
    private static final String DEFAULT_RND = "new";

    private final int min;
    private final int max;

    /**
     * Example request https://www.random.org/integers/?num=1&min=1&max=100&col=1&base=10&format=plain&rnd=new
     *
     * @return Integer
     */
    @Override
    public Integer getData() {

        String response = null;
        try {
            response = HttpRequestUtils.performGet(RANDOM_ORG_API_URL, getRequestParameters());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Integer.valueOf(String.valueOf(response));

    }

    private String getRequestParameters() throws UnsupportedEncodingException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("num", DEFAULT_NUM);
        parameters.put("min", String.valueOf(min));
        parameters.put("max", String.valueOf(max));
        parameters.put("col", DEFAULT_COL);
        parameters.put("base", DEFAULT_BASE);
        parameters.put("format", DEFAULT_FORMAT);
        parameters.put("rnd", DEFAULT_RND);
        return HttpRequestUtils.getParamsString(parameters);
    }
}
