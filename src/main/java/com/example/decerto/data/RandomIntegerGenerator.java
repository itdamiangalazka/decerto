package com.example.decerto.data;

import java.util.Random;

public class RandomIntegerGenerator implements CombinationDataSource<Integer> {

    @Override
    public Integer getData() {
        Random ran = new Random();
        return ran.nextInt(6) + 5; // parameters hardcoded to simplify
    }
}
