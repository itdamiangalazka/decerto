package com.example.decerto.data;

public interface CombinationDataSource<T>  {

    T getData();
}
