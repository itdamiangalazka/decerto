package com.example.decerto.data;

public abstract class CombinationResultDto {

    public abstract Object getValue();
}
